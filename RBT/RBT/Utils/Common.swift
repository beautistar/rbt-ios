//
//  String.swift
//  RBT
//
//  Created by Beautistar on 10/25/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "RBT"
        
        static let OK = "Ok"
        static let CANCEL = "Cancel"
        static let ERROR = "Error Occured!"
        static let CONNECT_FAIL = "Connection to the server failed.\nPlease try again."
        
        static let INPUT_FIRSTNAME = "Please input your first name."
        static let INPUT_LASTNAME = "Please input your last name."
        static let INPUT_EMAIL = "Please input your email address."
        static let INPUT_VALID_EMAIL = "Please input your valid email address."
        static let INPUT_PWD = "Please input more than 6 character password."
        static let MATCH_PWD = "Password does not match."
        static let CHK_TERM = "Please read and agree to terms and policy."
        
        static let USER_ALREADY = "User already registered."
        
        static let FINDING = "Finding location..."
        
    }
}

