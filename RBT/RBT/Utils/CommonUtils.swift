//
//  CommonUtils.swift
//  RBT
//
//  Created by Beautistar on 10/25/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

var updateTime = 15.0
var alertedAreaIds = [Int]()

func isValidEmail(testStr:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
    
}

