//
//  Const.swift
//  RBT
//
//  Created by Beautistar on 10/25/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation



struct Const {
    
    static let SAVE_ROOT_PATH = "RBT"
    
    static let BASE_URL = "http://18.216.24.138/index.php/api/"
    
    static let REQ_FBLOGIN = BASE_URL + "fblogin/"
    static let REQ_REGISTER = BASE_URL + "register/"
    static let REQ_LOGIN = BASE_URL + "login/"
    static let REQ_SETALERT = BASE_URL + "setAlert/"
    static let REQ_GETALERTLIST = BASE_URL + "getAlertList/"
    static let REQ_FINDALERTMAP = BASE_URL + "findAlertMap/"
    static let REQ_GETALERTS = BASE_URL + "getAlerts/"
    
    
    static let RES_RESULTCODE = "result_code"
    static let RES_USERID = "user_id"
    static let RES_FULLNAME = "full_name"
    static let RES_USERINFO = "user_info"
    
    static let RES_ALERTLIST = "alert_list"
    static let RES_ALERTCOUNT = "alert_count"
    static let RES_ID = "id"
    static let RES_ALERTTYPE = "alert_type"
    static let RES_LOCATION = "location"
    static let RES_SUBLOCATION = "sub_location"
    static let RES_LAT = "latitude"
    static let RES_LNG = "longitude"
    static let RES_MEMBERS = "members"
    static let RES_RECORDTIME = "record_time"
    static let RES_DISTANCE = "distance"
    
    
    static let CODE_SUCCESS = 0
    static let RES_MSG = "msg"
    
    static let TYPE_POLICE = 1
    static let TYPE_EMERGENCY = 2
    static let TYPE_FIRE = 3
    
    
}
