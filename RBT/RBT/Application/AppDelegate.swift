//
//  AppDelegate.swift
//  RBT
//
//  Created by Beautistar on 10/20/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var g_user = UserEntity()
    var locationManager = CLLocationManager()
    var geoCoder = CLGeocoder()

    var locationTimer = Timer()
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        initMenu()
        locationManager.delegate = self
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        ProgressHUD.initHUD()
        UIApplication.shared.isIdleTimerDisabled = true
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.badge], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        if #available(iOS 10.0, *) {
            
            if let info = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable:Any] {
                
                print("Background Message 1 ID: \(info["gcm.message_id"]!)")
            }
        }
        
        updateTimer()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        registerBackgroundTask()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        endBackgroundTask()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        locationManager.startUpdatingLocation()
        findLocation()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - User Defined Functions
    
    func updateTimer(){
        
        locationTimer = Timer.scheduledTimer(timeInterval: updateTime, target: self, selector: #selector(updateLocation), userInfo: nil, repeats: true)
    }
    
    @objc func updateLocation()
    {
        if self.g_user._id > 0 && self.g_user._latitude != 0 && self.g_user._longitude != 0{
            if UIApplication.shared.applicationState == .background, UIApplication.shared.backgroundTimeRemaining < updateTime {
                registerBackgroundTask()
            }
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let mapVC = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            mapVC.getAlertList()
        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        
        if locationTimer.isValid {
            locationTimer.invalidate()
        }
        updateTimer()
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
        
        if locationTimer.isValid {
            locationTimer.invalidate()
        }
        updateTimer()
    }
    
    func findLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if g_user._latitude == 0 && g_user._longitude == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationLoaded"), object: nil)
        }
       

        if let lastLocation = locations.first {
            
            self.g_user._latitude = (lastLocation.coordinate.latitude)
            self.g_user._longitude = (lastLocation.coordinate.longitude)
            
            geoCoder.reverseGeocodeLocation(lastLocation, completionHandler: {(placemarks, error)->Void in
                
                if (error != nil)
                {
                    print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    return
                }
                
                if (placemarks?.count)! > 0
                {
                    let pm = placemarks?[0]
                    self.displayLocationInfo(placemark: pm)
                }
                else
                {
                    print("Problem with the data received from geocoder")
                }
            })
        }
    }
    
    func displayLocationInfo(placemark: CLPlacemark?)
    {
        if let containsPlacemark = placemark
        {
            //stop updating location to save battery life
            //locationManager.stopUpdatingLocation()
            
            //let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            //let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            //let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            //let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            let thoroughfare = (containsPlacemark.thoroughfare != nil) ? containsPlacemark.thoroughfare : ""
            let sublocality = (containsPlacemark.subLocality != nil) ? containsPlacemark.subLocality : ""
            //let countryCode = (containsPlacemark.isoCountryCode != nil) ? containsPlacemark.isoCountryCode : ""
            /*
            print(locality ?? "")
            print(sublocality ?? "")
            print(thoroughfare ?? "")
            print(postalCode ?? "")
            print(administrativeArea ?? "")
            print(country ?? "")
            print(countryCode ?? "")
            
            print("location ======= " + locality!)
            print("sub location : \(sublocality!)")
            print("thoroughfare : \(thoroughfare!)")
            */
            self.g_user._location = sublocality ?? "unknown"
            self.g_user._sub_location = thoroughfare ?? "unknown"
        }
    }
    
    /// custom function
    
    func initMenu() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let leftMenu = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        let rightMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        SlideNavigationController.sharedInstance().rightMenu = rightMenu
        SlideNavigationController.sharedInstance().leftMenu = leftMenu
        SlideNavigationController.sharedInstance().menuRevealAnimationDuration = 0.2
        SlideNavigationController.sharedInstance().panGestureSideOffset = 0
        SlideNavigationController.sharedInstance().enableSwipeGesture = false
        SlideNavigationController.sharedInstance().enableShadow = false
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidClose), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Closed", menu)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidOpen), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Opened", menu)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidReveal), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Revealed", menu)
        }
    }
    
    class func getUser() -> UserEntity {
        
        let App : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        return App.g_user;
    }
    
    class func setUser(user: UserEntity) {
        let App : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        App.g_user = user
    }
}

