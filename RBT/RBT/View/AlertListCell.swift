//
//  AlertListCell.swift
//  RBT
//
//  Created by Beautistar on 10/24/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class AlertListCell: UITableViewCell {

    @IBOutlet weak var imgAlert: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblSubLocation: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMember: UILabel!
    @IBOutlet weak var btnClear: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
