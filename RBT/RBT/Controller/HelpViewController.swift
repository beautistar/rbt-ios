//
//  HelpViewController.swift
//  RBT
//
//  Created by Yin on 16/11/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        showHelp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHelp() {
        
        if let pdf = Bundle.main.url(forResource: "help", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            webView.loadRequest(req as URLRequest)
        }
    }
    
    @IBAction func menuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().open(MenuRight, withCompletion: nil)
    }
}
