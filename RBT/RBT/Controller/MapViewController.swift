//
//  MapViewController.swift
//  RBT
//
//  Created by Beautistar on 10/23/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON
import AudioToolbox

class MapViewController: BaseViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    
    var _alertInfos = [AlertEntity]()
    var annotations = [MKAnnotation]()
    var pins = [#imageLiteral(resourceName: "ic_map_police"), #imageLiteral(resourceName: "ic_map_emerg"), #imageLiteral(resourceName: "ic_map_fire")]
    var newAlert = false
 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.locationManager.delegate = self
        mapView.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.mapView.showsUserLocation = true
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let location = CLLocationCoordinate2DMake(AppDelegate.getUser()._latitude, AppDelegate.getUser()._longitude)
        self.mapView.camera.centerCoordinate = location
        let camera = MKMapCamera.init(lookingAtCenter: location, fromDistance: 5, pitch: 20, heading: 1)
        self.mapView.setCamera(camera, animated: true)
        self.mapView.contentScaleFactor = 15
        self.mapView.showsBuildings = true
        self.mapView.showsTraffic = true
        
        getAlertOnMap()
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.locationManager.startUpdatingLocation()
    }
    
    /// get alert within radius 50KM
    func getAlertOnMap() {
        
        _alertInfos.removeAll()
        annotations.removeAll()
        annotations.append(MKUserLocation.init())
        
        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "cur_lat")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "cur_lng")
        },
            to: Const.REQ_GETALERTLIST,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("GET alert response : ", response)
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                let alertCount = dict[Const.RES_ALERTCOUNT].intValue
                                
                                if UIApplication.shared.applicationIconBadgeNumber != alertCount {
                                    UIApplication.shared.applicationIconBadgeNumber = alertCount
                                }
                                
                                let alertDict = dict[Const.RES_ALERTLIST].arrayValue
                                
                                for alert in alertDict {
                                    
                                    let _alert = AlertEntity()
                                    _alert._id = alert[Const.RES_ID].intValue
                                    _alert._full_name = alert[Const.RES_FULLNAME].stringValue
                                    _alert._alert_type = alert[Const.RES_ALERTTYPE].intValue
                                    _alert._location = alert[Const.RES_LOCATION].stringValue
                                    _alert._sub_location = alert[Const.RES_SUBLOCATION].stringValue
                                    _alert._record_time = alert[Const.RES_RECORDTIME].stringValue
                                    _alert._members = alert[Const.RES_MEMBERS].intValue
                                    
                                    self._alertInfos.append(_alert)
                                    
                                    let _annotation = MKPointAnnotation.init()
                                    _annotation.coordinate = CLLocationCoordinate2DMake(alert[Const.RES_LAT].doubleValue, alert[Const.RES_LNG].doubleValue)
                                    _annotation.subtitle = "\(alert[Const.RES_ALERTTYPE].intValue)"
                                    //_annotation.title = _alert._location + " " + _alert._sub_location
                                    self.annotations.append(_annotation)
                                }
                            }
                            if self.mapView != nil {
                                self.mapView.addAnnotations(self.annotations)
                                self.mapView.reloadInputViews()
                            }
                            
                        } else  {
                            
                            self.showToast(R.string.CONNECT_FAIL)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
        
    }
   
    func getAlertList() {
        
        _alertInfos.removeAll()
        annotations.removeAll()
        annotations.append(MKUserLocation.init())
        
        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "cur_lat")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "cur_lng")
        },
            to: Const.REQ_FINDALERTMAP,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("GET alert response : ", response)
                        
                        if let result = response.result.value {
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                let alertCount = dict[Const.RES_ALERTCOUNT].intValue
                                
                                if UIApplication.shared.applicationIconBadgeNumber != alertCount {
                                    UIApplication.shared.applicationIconBadgeNumber = alertCount
                                }
                                
                                let alertDict = dict[Const.RES_ALERTLIST].arrayValue
                                
                                for alert in alertDict {
                                    
                                    let _alert = AlertEntity()
                                    _alert._id = alert[Const.RES_ID].intValue
                                    _alert._full_name = alert[Const.RES_FULLNAME].stringValue
                                    _alert._alert_type = alert[Const.RES_ALERTTYPE].intValue
                                    _alert._location = alert[Const.RES_LOCATION].stringValue
                                    _alert._sub_location = alert[Const.RES_SUBLOCATION].stringValue
                                    _alert._record_time = alert[Const.RES_RECORDTIME].stringValue
                                    _alert._members = alert[Const.RES_MEMBERS].intValue
                                    
                                    self._alertInfos.append(_alert)
                                    if !alertedAreaIds.contains(_alert._id) {
                                        alertedAreaIds.append(_alert._id)
                                        self.newAlert = true
                                    }
                                }
                                if self._alertInfos.count > 0 {
                                    if self.newAlert {
                                        self.siren()
                                        self.newAlert = false
                                    }
                                }
                            }
                        } else  {
                            
                            self.showToast(R.string.CONNECT_FAIL)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                }
        }
        )
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.04, longitudeDelta: 0.04))
        
        self.mapView.setRegion(region, animated: true)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors " + error.localizedDescription)
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            
        let annotationIdentifier = "userlocation"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            if (annotation is MKUserLocation) {
                annotationView.image = #imageLiteral(resourceName: "ic_map_car")
            } else {
                let index:Int = Int(annotation.subtitle! ?? "1")!
                annotationView.image = pins[index-1]
            }
        }
        return annotationView
    }
        
    
    func siren() {
        
        if let soundURL = Bundle.main.url(forResource: "siren", withExtension: "wav") {
            var mySound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
            // Play
            AudioServicesPlaySystemSound(mySound);
        }
        
    }
}

