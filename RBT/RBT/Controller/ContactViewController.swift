//
//  ContactViewController.swift
//  RBT
//
//  Created by Yin on 16/11/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: BaseViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var txvMessage: UITextView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        txvMessage.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        SlideNavigationController.sharedInstance().open(MenuRight, withCompletion: nil)
    }

    @IBAction func sendAction(_ sender: Any) {
        
        if tfEmail.text?.count == 0 || tfName.text?.count == 0 || txvMessage.text.count == 0 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: "Validation failed", positive: R.string.OK, negative: nil)
            return
        }
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["Support@randomblockedtraffic.com"])
            mail.setMessageBody("Name: " + tfName.text! + "<br>" +
                                "Email: " + tfEmail.text! + "<br>" +
                                "Message: " + txvMessage.text, isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
            self.showToast("You need to add your mail in phone setting")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
