//
//  MainTabViewController.swift
//  RBT
//
//  Created by Beautistar on 10/24/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import CoreLocation

class MainTabViewController: UITabBarController, UITabBarControllerDelegate, SlideNavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initTabbar()
        //initMenu()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initTabbar() {
        
        self.tabBarController?.delegate = self
        
        if let items = self.tabBar.items
        {
            for item in items
            {
                if let image = item.image
                {
                    item.image = image.withRenderingMode( .alwaysOriginal )
                    item.selectedImage = image.withRenderingMode(.alwaysOriginal)
                }
            }
        }
    }
    
    /// custom function
    
    func initMenu() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let leftMenu = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        let rightMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        SlideNavigationController.sharedInstance().rightMenu = rightMenu
        SlideNavigationController.sharedInstance().leftMenu = leftMenu
        SlideNavigationController.sharedInstance().menuRevealAnimationDuration = 0.2
        SlideNavigationController.sharedInstance().panGestureSideOffset = 0
        SlideNavigationController.sharedInstance().enableSwipeGesture = false
        SlideNavigationController.sharedInstance().enableShadow = false
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidClose), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Closed", menu)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidOpen), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Opened", menu)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidReveal), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Revealed", menu)
        }
    }
    
    
    
    
}
