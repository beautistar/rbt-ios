//
//  SignupViewController.swift
//  RBT
//
//  Created by Beautistar on 10/23/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

class SignupViewController: BaseViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfRePassword: UITextField!
    @IBOutlet weak var imgCheckBox: UIImageView!
    
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var haveButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    
    var isChecked = false
    
    var fb_id = ""
    var full_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkLastLogin()
    }
    
    func initView() {
        
          self.navigationController?.isNavigationBarHidden = true
        
        createButton.layer.shadowColor = UIColor.black.cgColor
        createButton.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        createButton.layer.masksToBounds = false
        createButton.layer.shadowRadius = 1.0
        createButton.layer.shadowOpacity = 0.5
        createButton.layer.cornerRadius = 5.0
        
        haveButton.layer.shadowColor = UIColor.black.cgColor
        haveButton.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        haveButton.layer.masksToBounds = false
        haveButton.layer.shadowRadius = 1.0
        haveButton.layer.shadowOpacity = 0.5
        haveButton.layer.cornerRadius = 5.0
        
        fbButton.layer.shadowColor = UIColor.black.cgColor
        fbButton.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        fbButton.layer.masksToBounds = false
        fbButton.layer.shadowRadius = 1.0
        fbButton.layer.shadowOpacity = 0.5
        fbButton.layer.cornerRadius = 5.0
        
        lblTitle.layer.shadowColor = UIColor.black.cgColor
        lblTitle.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        lblTitle.layer.masksToBounds = false
        lblTitle.layer.shadowRadius = 1.0
        lblTitle.layer.shadowOpacity = 0.5
        lblTitle.layer.cornerRadius = 5.0
        
        isChecked = false
        imgCheckBox.isHidden = true
    }
    
    
    func checkLastLogin() {
        
        if Defaults[.id] != 0 {
            AppDelegate.getUser()._id = Defaults[.id]
            AppDelegate.getUser()._full_name = Defaults[.full_name]
            gotoHome()
            
        }
    }
    
    @IBAction func fbLoginAction(_ sender: Any) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        
                        fbLoginManager.logOut()
                    }
                }
            }
        }
        
        
    }
    
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    self.fb_id = jsonResponse["id"].stringValue
                    self.full_name = jsonResponse["name"].string ?? "unknown"
                    
                    print("id and name", self.fb_id, self.full_name)
                    
                    self.fbLogin()
                    
                } else {
                    // TODO :  Exeption  ****//504724896555224
                    print(error!)
                    
                }
            })
        } else {
            print("token is null")
        }
    }
    
    func fbLogin() {
        
        self.showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(self.fb_id.data(using:String.Encoding.utf8)!, withName: "fb_id")
                multipartFormData.append(self.full_name.data(using:String.Encoding.utf8)!, withName: "full_name")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "cur_lat")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "cur_lng")
                
        },
            to: Const.REQ_FBLOGIN,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("register response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                AppDelegate.getUser()._id = dict[Const.RES_USERINFO][Const.RES_USERID].intValue
                                AppDelegate.getUser()._full_name = self.full_name
                                
                                Defaults[.id] = AppDelegate.getUser()._id
                                Defaults[.full_name] = AppDelegate.getUser()._full_name
                                
                                /// go to main page
                                self.gotoHome()
                                
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
    }
    
    @IBAction func checlAction(_ sender: Any) {
        
        isChecked = !isChecked
        
        if isChecked {
            imgCheckBox.isHidden = false
        } else {
            imgCheckBox.isHidden = true
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {
        
        if checkValid() {
        
            self.register()
            
        }
    }
    
    func gotoHome() {
        
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        //UIApplication.shared.keyWindow?.rootViewController = homeVC
        //UIApplication.shared.keyWindow?.makeKeyAndVisible()
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    
    func checkValid() -> Bool {
        
        if (tfFirstName.text?.isEmpty)! || (tfFirstName.text?.count)! == 0 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_FIRSTNAME, positive: nil, negative: R.string.OK)
            return false
        }
        
        if (tfLastName.text?.isEmpty)! || (tfLastName.text?.count)! == 0 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_LASTNAME, positive: nil, negative: R.string.OK)
            return false
        }
        
        if (tfEmail.text?.isEmpty)! || tfEmail.text?.count == 0 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_EMAIL, positive: nil, negative: R.string.OK)
            return false
        }
        
        if !isValidEmail(testStr: tfEmail.text!) {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_VALID_EMAIL, positive: nil, negative: R.string.OK)
            return false
        }
        
        if (tfPassword.text?.isEmpty)! || (tfPassword.text?.count)! < 6 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_PWD, positive: nil, negative: R.string.OK)
            return false
        }
        
        if (tfRePassword.text?.isEmpty)! || (tfRePassword.text?.count)! == 0 || tfRePassword.text != tfPassword.text {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.MATCH_PWD, positive: nil, negative: R.string.OK)
            return false
        }
        
        if !isChecked {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.CHK_TERM, positive: nil, negative: R.string.OK)
            return false
        }
        
        return true
    }    
    
    func register() {
        
        let full_name = tfFirstName.text! + " " + tfLastName.text!
        let email = self.tfEmail.text!
        let password = self.tfPassword.text!
        self.showLoadingView()
        
        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                
                multipartFormData.append(email.data(using:String.Encoding.utf8)!, withName: "email")
                multipartFormData.append(full_name.data(using:String.Encoding.utf8)!, withName: "full_name")
                multipartFormData.append(password.data(using:String.Encoding.utf8)!, withName: "password")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "cur_lat")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "cur_lng")
                
        },
            to: Const.REQ_REGISTER,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("register response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                AppDelegate.getUser()._id = dict[Const.RES_USERID].intValue
                                AppDelegate.getUser()._full_name = full_name
                                
                                Defaults[.id] = AppDelegate.getUser()._id
                                Defaults[.full_name] = AppDelegate.getUser()._full_name
                                
                                /// go to main page
                                self.gotoHome()
                            } else {
                                self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.USER_ALREADY, positive: nil, negative: R.string.OK
                                )
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
    }
    
    @IBAction func tapped(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
}
