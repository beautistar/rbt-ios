//
//  AlertListViewController.swift
//  RBT
//
//  Created by Beautistar on 10/23/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AlertListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblAlertList: UITableView!
    @IBOutlet weak var lblCurrentAlert: UILabel!
    
    
    var _alertInfos = [AlertEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.selectedIndex = 1
        getAlertList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func initView() {
        
        lblCurrentAlert.layer.shadowColor = UIColor.black.cgColor
        lblCurrentAlert.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        lblCurrentAlert.layer.masksToBounds = false
        lblCurrentAlert.layer.shadowRadius = 1.0
        lblCurrentAlert.layer.shadowOpacity = 0.5
        lblCurrentAlert.layer.cornerRadius = 5.0
    }
    
    
    func getAlertList() {
        
        _alertInfos.removeAll()
        
        let distance = 50.0
        
        self.showLoadingView()
        
        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(distance)".data(using:String.Encoding.utf8)!, withName: "distance")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "cur_lat")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "cur_lng")
        },
            to: Const.REQ_GETALERTS,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("GET alert response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                
                                let alertDict = dict[Const.RES_ALERTLIST].arrayValue
                                
                                for alert in alertDict {
                                    
                                    let _alert = AlertEntity()
                                    _alert._id = alert[Const.RES_ID].intValue
                                    _alert._full_name = alert[Const.RES_FULLNAME].stringValue
                                    _alert._alert_type = alert[Const.RES_ALERTTYPE].intValue
                                    _alert._location = alert[Const.RES_LOCATION].stringValue
                                    _alert._sub_location = alert[Const.RES_SUBLOCATION].stringValue
                                    _alert._record_time = alert[Const.RES_RECORDTIME].stringValue
                                    _alert._members = alert[Const.RES_MEMBERS].intValue
                                    
                                    self._alertInfos.append(_alert)
                                    
                                    self.tblAlertList.reloadData()
                                    
                                }                               
                                
                            } else {
                                self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.USER_ALREADY, positive: nil, negative: R.string.OK
                                )
                            }
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
    }   
    
    
    // MARK: - TableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return _alertInfos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let selectedAlert = _alertInfos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlertListCell") as! AlertListCell
        switch selectedAlert._alert_type {
        case Const.TYPE_POLICE:
            cell.imgAlert.image = #imageLiteral(resourceName: "ic_police_large")
            break
        case Const.TYPE_EMERGENCY:
            cell.imgAlert.image = #imageLiteral(resourceName: "ic_emer_large")
            break
        default:
            cell.imgAlert.image = #imageLiteral(resourceName: "ic_fire_large")
        }
        cell.lblLocation.text = "location: " + selectedAlert._location.lowercased()
        cell.lblSubLocation.text = "suburb: " + selectedAlert._sub_location.lowercased()
        cell.lblMember.text = "number of member reports: " + String(selectedAlert._members)
        
        // create dateFormatter with UTC time format
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
        formatter.timeZone = TimeZone(identifier:"UTC")
        //formatter.locale = Locale.init(identifier: "en_GB")
        let _date = formatter.date(from : selectedAlert._record_time)!
        
        // change to a readable time format and change to local time zone
        formatter.dateFormat = "h:mma"
        formatter.timeZone = NSTimeZone.local
        let dateString = formatter.string(from: _date)
        cell.lblTime.text = "time reported: " + dateString
        cell.btnClear.tag = indexPath.row
        cell.btnClear.addTarget(self, action: #selector(clearAlert(_:)), for: .touchUpInside)
        return cell

    }
    
    @objc func clearAlert(_ sender:UIButton) {
        
        _alertInfos.remove(at: sender.tag)
        tblAlertList.reloadData()
        
    }
}
