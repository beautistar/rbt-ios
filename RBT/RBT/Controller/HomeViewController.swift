//
//  HomeViewController.swift
//  RBT
//
//  Created by Beautistar on 10/23/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import AudioToolbox

class HomeViewController: BaseViewController, SlideNavigationControllerDelegate {

    @IBOutlet weak var imgPolice: UIImageView!
    @IBOutlet weak var imgEmerg: UIImageView!
    @IBOutlet weak var imgFire: UIImageView!
    @IBOutlet weak var popView: UIView!
    var geoCoder = CLGeocoder()
    var alert_type = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        self.navigationController?.isNavigationBarHidden = true
        
        imgFire.layer.borderColor = UIColor.white.cgColor
        imgEmerg.layer.borderColor = UIColor.white.cgColor
        imgPolice.layer.borderColor = UIColor.white.cgColor
        
        popView.layer.borderColor = UIColor.white.cgColor
        popView.isHidden = true
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        findAddress()
    }
    
    func findAddress() {
        
        let curLocation:CLLocation = CLLocation.init(latitude: AppDelegate.getUser()._latitude, longitude: AppDelegate.getUser()._longitude)
        
        geoCoder.reverseGeocodeLocation(curLocation, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil)
            {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0
            {
                let pm = placemarks?[0]
                self.displayLocationInfo(placemark: pm)
            }
            else
            {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark?)
    {
        if let containsPlacemark = placemark
        {
            //stop updating location to save battery life
            //locationManager.stopUpdatingLocation()
            
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            let thoroughfare = (containsPlacemark.thoroughfare != nil) ? containsPlacemark.thoroughfare : ""
            let sublocality = (containsPlacemark.subLocality != nil) ? containsPlacemark.subLocality : ""
            let countryCode = (containsPlacemark.isoCountryCode != nil) ? containsPlacemark.isoCountryCode : ""
            
            print(locality ?? "")
            print(sublocality ?? "")
            print(thoroughfare ?? "")
            print(postalCode ?? "")
            print(administrativeArea ?? "")
            print(country ?? "")
            print(countryCode ?? "")
            
            print("location ======= " + locality!)
            print("sub location : \(sublocality!)")
            print("thoroughfare : \(thoroughfare!)")
            
            AppDelegate.getUser()._location = sublocality ?? "unknown"
            AppDelegate.getUser()._sub_location = thoroughfare ?? "unknown"
        }
    }
    
    @IBAction func alertAction(_ sender: UIButton) {
        
        alert_type = sender.tag
        
        if AppDelegate.getUser()._sub_location.count != 0 && AppDelegate.getUser()._location.count != 0 {
            setAlert(type: alert_type)
            setAlertSound()
            self.showToastAlert()
        } else {
            self.showToast(R.string.FINDING)
        }
    }
    @IBAction func menuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().open(MenuRight, withCompletion: nil)
    }
    
    func showToastAlert() {
        
        popView.isHidden = false
        popView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.popView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
            self.perform(#selector(self.popViewHide), with: nil, afterDelay: 4);
        })
    }
    
    @objc func popViewHide() {
        popView.isHidden = true
    }
    
    func setAlertSound() {
        
        if let soundURL = Bundle.main.url(forResource: "alert", withExtension: "wav") {
            var mySound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
            // Play
            AudioServicesPlaySystemSound(mySound);
        }
        
    }
    
    func setAlert(type: Int) {
        
        //self.showLoadingView()
        
        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(type)".data(using:String.Encoding.utf8)!, withName: "type")
                multipartFormData.append("\(AppDelegate.getUser()._full_name)".data(using:String.Encoding.utf8)!, withName: "full_name")
                multipartFormData.append("\(AppDelegate.getUser()._location)".data(using:String.Encoding.utf8)!, withName: "location")
                multipartFormData.append("\(AppDelegate.getUser()._sub_location)".data(using:String.Encoding.utf8)!, withName: "sub_location")
                multipartFormData.append("\(AppDelegate.getUser()._latitude)".data(using:String.Encoding.utf8)!, withName: "cur_lat")
                multipartFormData.append("\(AppDelegate.getUser()._longitude)".data(using:String.Encoding.utf8)!, withName: "cur_lng")
        },
            to: Const.REQ_SETALERT,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("ser alert response : ", response)
                        
                        if let result = response.result.value {
                            
                            //self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                //self.showToast(dict[Const.RES_MSG].stringValue)
                                
                                
                            } else {
                                self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.USER_ALREADY, positive: nil, negative: R.string.OK
                                )
                            }
                        } else  {
                            
                            //self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                    
                }
        }
        )
    }
}

