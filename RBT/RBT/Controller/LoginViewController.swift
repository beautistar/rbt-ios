//
//  LoginViewController.swift
//  RBT
//
//  Created by Beautistar on 10/23/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

class LoginViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var loginButton: UIButton!
    
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func initView() {      
        
        loginButton.layer.shadowColor = UIColor.black.cgColor
        loginButton.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        loginButton.layer.masksToBounds = false
        loginButton.layer.shadowRadius = 1.0
        loginButton.layer.shadowOpacity = 0.5
        loginButton.layer.cornerRadius = 5.0
        

    }
    
    @IBAction func loginAction(_ sender: Any) {
    
        if checkValid() {
            self.login()
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func checkValid() -> Bool {
        
        if (tfEmail.text?.isEmpty)! || tfEmail.text?.count == 0 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_EMAIL, positive: nil, negative: R.string.OK)
            return false
        }
        
        if !isValidEmail(testStr: tfEmail.text!) {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_VALID_EMAIL, positive: nil, negative: R.string.OK)
            return false
        }
        
        if (tfPassword.text?.isEmpty)! || (tfPassword.text?.count)! < 6 {
            self.showAlertDialog(title: R.string.APP_TITLE, message: R.string.INPUT_PWD, positive: nil, negative: R.string.OK)
            return false
        }
        
        return true
        
    }
    
    func login() {
        
        self.showLoadingView()
        
        Alamofire.upload(
            
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(self.tfEmail.text!)".data(using:String.Encoding.utf8)!, withName: "email")
                multipartFormData.append("\(self.tfPassword.text!)".data(using:String.Encoding.utf8)!, withName: "password")
        },
            to: Const.REQ_LOGIN,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print("login response : ", response)
                        
                        if let result = response.result.value {
                            
                            self.hideLoadingView()
                            
                            let dict = JSON(result)
                            
                            let result_code = dict[Const.RES_RESULTCODE].intValue
                            
                            if result_code == Const.CODE_SUCCESS {
                                
                                AppDelegate.getUser()._id = dict[Const.RES_USERINFO][Const.RES_USERID].intValue
                                AppDelegate.getUser()._full_name = dict[Const.RES_USERINFO][Const.RES_FULLNAME].stringValue
                                
                                Defaults[.id] = AppDelegate.getUser()._id
                                Defaults[.full_name] = AppDelegate.getUser()._full_name
                                
                                /// go to main page
                                self.gotoHome()
                                
                            } else  {
                                self.showAlertDialog(title: R.string.APP_TITLE, message: dict[Const.RES_MSG].stringValue, positive: nil, negative: R.string.OK
                                )
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.hideLoadingView()
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    return
                }
        }
        )
    }
        
    func gotoHome() {
        
        self.dismiss(animated: false, completion: nil)
        
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        //UIApplication.shared.keyWindow?.rootViewController = homeVC
        //UIApplication.shared.keyWindow?.makeKeyAndVisible()
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    
    //MARK:- TextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        return true
    }
}
