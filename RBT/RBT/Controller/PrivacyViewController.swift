//
//  PrivacyViewController.swift
//  RBT
//
//  Created by Yin on 31/10/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class PrivacyViewController: UIViewController, SlideNavigationControllerDelegate {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if Defaults[.id] > 0 {
            closeButton.isHidden = true
        } else {
            menuButton.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        SlideNavigationController.sharedInstance().open(MenuRight, withCompletion: nil)
    }
 }
