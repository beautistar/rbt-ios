//
//  MenuViewController.swift
//  RBT
//
//  Created by Yin on 16/11/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

enum RightMenu: Int {
    case home = 0
    case help
    case privacy
    case term
    case contact
}

class MenuViewController: UIViewController, SlideNavigationControllerDelegate {

    
    var mainViewController: UIViewController!
    var helpViewController: UIViewController!
    var privacyViewController: UIViewController!
    var termsViewController: UIViewController!
    var contactViewController: UIViewController!
    
    var slideOutAnimationEnabled:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        slideOutAnimationEnabled = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        mainViewController = storyboard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        helpViewController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        privacyViewController = storyboard.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        termsViewController = storyboard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        contactViewController = storyboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        
        changeViewController(RightMenu(rawValue: sender.tag)!)
    }
    
    func changeViewController(_ menu: RightMenu) {
        
        switch menu {
            
        case .home:
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: self.mainViewController, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        case .help:
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: self.helpViewController, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        case .privacy:
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: self.privacyViewController, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        case .term:
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: self.termsViewController, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        case .contact:
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: self.contactViewController, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        }    
    }
    
    // MARK: - SlideNavigationController Methods
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
}


