//
//  AlertEntity.swift
//  RBT
//
//  Created by Beautistar on 10/26/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class AlertEntity {
    
    var _id = 0
    var _full_name = ""
    var _user_id = 0
    var _latitude = 0.0
    var _longitude = 0.0
    var _location = "unknown"
    var _sub_location = "unknown"
    var _alert_type = 0
    var _members = 0
    var _record_time = ""
}
