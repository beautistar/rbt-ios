//
//  UserEntity.swift
//  RBT
//
//  Created by Beautistar on 10/24/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class UserEntity {
    
    var _id = 0
    var _full_name = ""
    var _email = ""
    var _latitude = 0.0
    var _longitude = 0.0
    var _location = ""
    var _sub_location = ""
}
